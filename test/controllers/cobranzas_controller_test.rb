require 'test_helper'

class CobranzasControllerTest < ActionDispatch::IntegrationTest
  test "should get list" do
    get cobranzas_list_url
    assert_response :success
  end

  test "should get new" do
    get cobranzas_new_url
    assert_response :success
  end

end
