require 'test_helper'

class ReportesControllerTest < ActionDispatch::IntegrationTest
  test "should get ventas" do
    get reportes_ventas_url
    assert_response :success
  end

  test "should get cobros" do
    get reportes_cobros_url
    assert_response :success
  end

  test "should get comisiones" do
    get reportes_comisiones_url
    assert_response :success
  end

  test "should get estado" do
    get reportes_estado_url
    assert_response :success
  end

  test "should get general" do
    get reportes_general_url
    assert_response :success
  end

  test "should get almacen" do
    get reportes_almacen_url
    assert_response :success
  end

  test "should get reparaciones" do
    get reportes_reparaciones_url
    assert_response :success
  end

  test "should get alertas" do
    get reportes_alertas_url
    assert_response :success
  end

  test "should get logs" do
    get reportes_logs_url
    assert_response :success
  end

end
