require 'test_helper'

class ConsortiaControllerTest < ActionDispatch::IntegrationTest
  setup do
    @consortium = consortia(:one)
  end

  test "should get index" do
    get consortia_url
    assert_response :success
  end

  test "should get new" do
    get new_consortium_url
    assert_response :success
  end

  test "should create consortium" do
    assert_difference('Consortium.count') do
      post consortia_url, params: { consortium: { comision_p: @consortium.comision_p, description: @consortium.description, personal: @consortium.personal, phone: @consortium.phone, status: @consortium.status, user_id: @consortium.user_id } }
    end

    assert_redirected_to consortium_url(Consortium.last)
  end

  test "should show consortium" do
    get consortium_url(@consortium)
    assert_response :success
  end

  test "should get edit" do
    get edit_consortium_url(@consortium)
    assert_response :success
  end

  test "should update consortium" do
    patch consortium_url(@consortium), params: { consortium: { comision_p: @consortium.comision_p, description: @consortium.description, personal: @consortium.personal, phone: @consortium.phone, status: @consortium.status, user_id: @consortium.user_id } }
    assert_redirected_to consortium_url(@consortium)
  end

  test "should destroy consortium" do
    assert_difference('Consortium.count', -1) do
      delete consortium_url(@consortium)
    end

    assert_redirected_to consortia_url
  end
end
