require 'test_helper'

class NotifyControllerTest < ActionDispatch::IntegrationTest
  test "should get list" do
    get notify_list_url
    assert_response :success
  end

  test "should get show" do
    get notify_show_url
    assert_response :success
  end

end
