require 'test_helper'

class CobradoresControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cobradore = cobradores(:one)
  end

  test "should get index" do
    get cobradores_url
    assert_response :success
  end

  test "should get new" do
    get new_cobradore_url
    assert_response :success
  end

  test "should create cobradore" do
    assert_difference('Cobradore.count') do
      post cobradores_url, params: { cobradore: { address: @cobradore.address, name: @cobradore.name, phone: @cobradore.phone, status: @cobradore.status, user_id: @cobradore.user_id } }
    end

    assert_redirected_to cobradore_url(Cobradore.last)
  end

  test "should show cobradore" do
    get cobradore_url(@cobradore)
    assert_response :success
  end

  test "should get edit" do
    get edit_cobradore_url(@cobradore)
    assert_response :success
  end

  test "should update cobradore" do
    patch cobradore_url(@cobradore), params: { cobradore: { address: @cobradore.address, name: @cobradore.name, phone: @cobradore.phone, status: @cobradore.status, user_id: @cobradore.user_id } }
    assert_redirected_to cobradore_url(@cobradore)
  end

  test "should destroy cobradore" do
    assert_difference('Cobradore.count', -1) do
      delete cobradore_url(@cobradore)
    end

    assert_redirected_to cobradores_url
  end
end
