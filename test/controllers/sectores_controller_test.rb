require 'test_helper'

class SectoresControllerTest < ActionDispatch::IntegrationTest
  setup do
    @sectore = sectores(:one)
  end

  test "should get index" do
    get sectores_url
    assert_response :success
  end

  test "should get new" do
    get new_sectore_url
    assert_response :success
  end

  test "should create sectore" do
    assert_difference('Sectore.count') do
      post sectores_url, params: { sectore: { description: @sectore.description, status: @sectore.status, user_id: @sectore.user_id } }
    end

    assert_redirected_to sectore_url(Sectore.last)
  end

  test "should show sectore" do
    get sectore_url(@sectore)
    assert_response :success
  end

  test "should get edit" do
    get edit_sectore_url(@sectore)
    assert_response :success
  end

  test "should update sectore" do
    patch sectore_url(@sectore), params: { sectore: { description: @sectore.description, status: @sectore.status, user_id: @sectore.user_id } }
    assert_redirected_to sectore_url(@sectore)
  end

  test "should destroy sectore" do
    assert_difference('Sectore.count', -1) do
      delete sectore_url(@sectore)
    end

    assert_redirected_to sectores_url
  end
end
