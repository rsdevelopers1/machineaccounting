require "application_system_test_case"

class CobradoresTest < ApplicationSystemTestCase
  setup do
    @cobradore = cobradores(:one)
  end

  test "visiting the index" do
    visit cobradores_url
    assert_selector "h1", text: "Cobradores"
  end

  test "creating a Cobradore" do
    visit cobradores_url
    click_on "New Cobradore"

    fill_in "Address", with: @cobradore.address
    fill_in "Name", with: @cobradore.name
    fill_in "Phone", with: @cobradore.phone
    fill_in "Status", with: @cobradore.status
    fill_in "User", with: @cobradore.user_id
    click_on "Create Cobradore"

    assert_text "Cobradore was successfully created"
    click_on "Back"
  end

  test "updating a Cobradore" do
    visit cobradores_url
    click_on "Edit", match: :first

    fill_in "Address", with: @cobradore.address
    fill_in "Name", with: @cobradore.name
    fill_in "Phone", with: @cobradore.phone
    fill_in "Status", with: @cobradore.status
    fill_in "User", with: @cobradore.user_id
    click_on "Update Cobradore"

    assert_text "Cobradore was successfully updated"
    click_on "Back"
  end

  test "destroying a Cobradore" do
    visit cobradores_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Cobradore was successfully destroyed"
  end
end
