require "application_system_test_case"

class SectoresTest < ApplicationSystemTestCase
  setup do
    @sectore = sectores(:one)
  end

  test "visiting the index" do
    visit sectores_url
    assert_selector "h1", text: "Sectores"
  end

  test "creating a Sectore" do
    visit sectores_url
    click_on "New Sectore"

    fill_in "Description", with: @sectore.description
    fill_in "Status", with: @sectore.status
    fill_in "User", with: @sectore.user_id
    click_on "Create Sectore"

    assert_text "Sectore was successfully created"
    click_on "Back"
  end

  test "updating a Sectore" do
    visit sectores_url
    click_on "Edit", match: :first

    fill_in "Description", with: @sectore.description
    fill_in "Status", with: @sectore.status
    fill_in "User", with: @sectore.user_id
    click_on "Update Sectore"

    assert_text "Sectore was successfully updated"
    click_on "Back"
  end

  test "destroying a Sectore" do
    visit sectores_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Sectore was successfully destroyed"
  end
end
