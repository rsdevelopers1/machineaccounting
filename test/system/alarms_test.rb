require "application_system_test_case"

class AlarmsTest < ApplicationSystemTestCase
  setup do
    @alarm = alarms(:one)
  end

  test "visiting the index" do
    visit alarms_url
    assert_selector "h1", text: "Alarms"
  end

  test "creating a Alarm" do
    visit alarms_url
    click_on "New Alarm"

    fill_in "Dat", with: @alarm.dat
    fill_in "Description", with: @alarm.description
    fill_in "Fri", with: @alarm.fri
    fill_in "Mon", with: @alarm.mon
    fill_in "Status", with: @alarm.status
    fill_in "Sun", with: @alarm.sun
    fill_in "Thu", with: @alarm.thu
    fill_in "Tue", with: @alarm.tue
    fill_in "User", with: @alarm.user_id
    fill_in "Wed", with: @alarm.wed
    click_on "Create Alarm"

    assert_text "Alarm was successfully created"
    click_on "Back"
  end

  test "updating a Alarm" do
    visit alarms_url
    click_on "Edit", match: :first

    fill_in "Dat", with: @alarm.dat
    fill_in "Description", with: @alarm.description
    fill_in "Fri", with: @alarm.fri
    fill_in "Mon", with: @alarm.mon
    fill_in "Status", with: @alarm.status
    fill_in "Sun", with: @alarm.sun
    fill_in "Thu", with: @alarm.thu
    fill_in "Tue", with: @alarm.tue
    fill_in "User", with: @alarm.user_id
    fill_in "Wed", with: @alarm.wed
    click_on "Update Alarm"

    assert_text "Alarm was successfully updated"
    click_on "Back"
  end

  test "destroying a Alarm" do
    visit alarms_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Alarm was successfully destroyed"
  end
end
