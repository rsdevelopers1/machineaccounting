require "application_system_test_case"

class ConsortiaTest < ApplicationSystemTestCase
  setup do
    @consortium = consortia(:one)
  end

  test "visiting the index" do
    visit consortia_url
    assert_selector "h1", text: "Consortia"
  end

  test "creating a Consortium" do
    visit consortia_url
    click_on "New Consortium"

    fill_in "Comision p", with: @consortium.comision_p
    fill_in "Description", with: @consortium.description
    fill_in "Personal", with: @consortium.personal
    fill_in "Phone", with: @consortium.phone
    fill_in "Status", with: @consortium.status
    fill_in "User", with: @consortium.user_id
    click_on "Create Consortium"

    assert_text "Consortium was successfully created"
    click_on "Back"
  end

  test "updating a Consortium" do
    visit consortia_url
    click_on "Edit", match: :first

    fill_in "Comision p", with: @consortium.comision_p
    fill_in "Description", with: @consortium.description
    fill_in "Personal", with: @consortium.personal
    fill_in "Phone", with: @consortium.phone
    fill_in "Status", with: @consortium.status
    fill_in "User", with: @consortium.user_id
    click_on "Update Consortium"

    assert_text "Consortium was successfully updated"
    click_on "Back"
  end

  test "destroying a Consortium" do
    visit consortia_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Consortium was successfully destroyed"
  end
end
