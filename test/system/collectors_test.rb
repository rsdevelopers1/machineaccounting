require "application_system_test_case"

class CollectorsTest < ApplicationSystemTestCase
  setup do
    @collector = collectors(:one)
  end

  test "visiting the index" do
    visit collectors_url
    assert_selector "h1", text: "Collectors"
  end

  test "creating a Collector" do
    visit collectors_url
    click_on "New Collector"

    fill_in "Address", with: @collector.address
    fill_in "Name", with: @collector.name
    fill_in "Phone", with: @collector.phone
    fill_in "Status", with: @collector.status
    fill_in "User", with: @collector.user_id
    click_on "Create Collector"

    assert_text "Collector was successfully created"
    click_on "Back"
  end

  test "updating a Collector" do
    visit collectors_url
    click_on "Edit", match: :first

    fill_in "Address", with: @collector.address
    fill_in "Name", with: @collector.name
    fill_in "Phone", with: @collector.phone
    fill_in "Status", with: @collector.status
    fill_in "User", with: @collector.user_id
    click_on "Update Collector"

    assert_text "Collector was successfully updated"
    click_on "Back"
  end

  test "destroying a Collector" do
    visit collectors_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Collector was successfully destroyed"
  end
end
