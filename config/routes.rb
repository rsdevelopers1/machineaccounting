Rails.application.routes.draw do
  get 'bloqueos/index'
  mount ActionCable.server => "/cable"
  get 'cobranzas/list'
  get 'cobranzas/new'
  resources :loans
  get 'loans/list'
  get 'loans/create'
  get 'loans/show'
  get 'reportes/ventas'
  get 'reportes/cobros'
  get 'reportes/comisiones'
  get 'reportes/estado'
  get 'reportes/general'
  get 'reportes/almacen'
  get 'reportes/reparaciones'
  get 'reportes/alertas'
  get 'reportes/logs'
  get 'notify/list'
  get 'notify/show/:id', to: "notify#show"
  get '/deudas', to: "cobranzas#deudas"
  resources :alarms
  resources :repairs
  get 'panel/init'
  get 'interative/maps'
  resources :cobros
  resources :pos
  resources :shippings do
    get :barcode, on: :member
    get :entrace, on: :member
    get :goware, on: :member
    get :modified, on: :member

  end
  resources :products
  get 'estadistics/welcome'
  resources :providers
  resources :consortia
  resources :categories
  resources :sectores
  resources :warehouses
  resources :cobradores
  resources :collectors
  resources :modelos
  resources :brands
  resources :zones
  devise_for :users
  root 'welcome#index'

  # Filtros
  get '/products_q', to: "products#search"
  get '/shippings_q', to: "shippings#search"
  get '/alerts_q', to: "alarms#alerts_q"
  get '/consortia_q', to: "consortia#consortia_q"
  get '/loan_q', to: "loans#loan_q"

  # Filtro Reporte
  get '/general_q', to: "reportes#generalQ"
  get '/reparaciones_q', to: "reportes#reparaciones_q"
  get '/logs_q', to: "reportes#logsQ"
  get '/notify_q', to: "notify#notify_q"
  get '/alertas_q', to: "reportes#alertas_q"
  get '/ventas_q', to: "reportes#ventas_q"
  get '/cobros_q', to: "reportes#cobros_q"
  get '/estado_q', to: "reportes#estado_q"
  get '/comisiones_q', to: "reportes#comisiones_q"
  # Saves
  post 'shipping/save', to: "shippings#save"


  namespace :api do

    #==============================================================>
    # Externas:
    #==============================================================>
      #Entrada de efectivo
      get 'setBillIn/:pos/:amount', to: "app#setBillIn"

      #Salida de efectivo
      get 'setBillOut/:pos/:amount', to: "app#setBillOut"

      #Apagar Maquina
      get 'shutdown/:pos', to: "app#shutdown"

      #Encender Maquina
      get 'poweron/:pos', to: "app#powerOn"

      #Buscar la localizacion de la maquina
      get 'getLocation/:pos', to: "app#getLocation"

      #Reportar aproximidad
      get 'setAproximidad/:pos', to: "app#setAproximidad"

      #Reportar Apagado
      get 'setShutDown/:pos', to: "app#setShutDown"

      #Reportar Encendido
      get 'setPowerOn/:pos', to: "app#setPoweOn"

      #Reportar conexion de wifi
      get 'setWifiOn/:pos', to: "app#setWifiOn"

      #Reportar desconexion de wifi
      get 'setWifiOff/:pos', to: "app#setWifiOff"

      #Reportar conexion a datos
      get 'setDataOn/:pos', to: "app#setDataOn"
      
      #Reportar desconexion a datos
      get 'setDataOff/:pos', to: "app#setDataOff"

      #Reporte de localizacion
      get 'setLocations/:pos/:lon/:lat', to: "app#setLocations"

      #Traer la configuracion
      get 'getConfigByNumber/:number', to: "app#getConfigByNumber"
    #==============================================================>
    # TEST
    #==============================================================>
    get 'test/poweroff', to: "app#POWEROFF_TEST"
    get 'test/poweron', to: "app#POWERON_TEST"


    #==============================================================>
    # Internal APIs
    #==============================================================>
    #Traer Prestamos pendientes
    #get 'getPrestamosPendientes/:pos', to: "app#getPrestamosPendientes"

    #Traer direcciones
    get 'getDirections/:zone/:status', to: "app#getDirections"

    # Traer controles
    get 'getControls/:pos', to: "app#getControl"

    # Buscar entrada especifica en almacen
    get 'getEntrance/:q_for/:ref', to: "app#getEntrance"

    # Actualizar Cambios a una entrada en especifico
    get 'changeEntrace/:id/:barcode/:ref', to: "app#changeEntrace"

    # Validando un dispositivo
    post 'CheckDevice/:barcode/:t', to: "app#checkDevice"

    # Metodo para traer las zonas de un cobrador
    post 'getCollectorZone/:c_id', to: "app#getCollectorZone"

    # Metodo para traer POS por zonas
    get 'getPOSZone/:z_id', to: "app#getPOSZone"

    # Metodo para traer zonas
    get 'getZones/', to: "app#getZones"

    # Metodo para traer zonas
    get 'getCasinos/', to: "app#getCasinos"

    # Metodo para traer deuda por POS
    get 'getDebt/:pos', to: "app#getDebt"
    
    #Metodo para traer las zonas de los cobraodres
    get 'getZoneByCobrador/:cobrador', to: "app#getZoneByCobrador"
    #==============================================================>
    #                      CRONS
    #==============================================================>
    get '/crons/GHWK5_Alarms/:token', to: "app#cron_alarms"
    get '/crons/GSWT5_Hours/:token', to: "app#cron_hours"
    get '/crons/GVFT5_Mora/:token', to: "app#cron_moras"

    #==============================================================>
    #                      Estadisticas
    #==============================================================>
    get '/estadisticas/ventas/diaria', to: "app#estVentasDiaria"
    
    #==============================================================>
  
  end

  namespace :internal do

    #verificando dispositvos
    get 'CheckDevice/:barcode/:t', to:"internal#checkDevice"

  end
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
