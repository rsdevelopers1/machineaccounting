json.extract! category, :id, :user_id, :description, :status, :created_at, :updated_at
json.url category_url(category, format: :json)
