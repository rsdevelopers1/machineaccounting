json.extract! modelo, :id, :user_id, :brand_id, :description, :status, :created_at, :updated_at
json.url modelo_url(modelo, format: :json)
