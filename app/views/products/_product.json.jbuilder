json.extract! product, :id, :user_id, :type_p, :brand_id, :modelo_id, :category_id, :created_at, :updated_at
json.url product_url(product, format: :json)
