json.extract! sectore, :id, :user_id, :description, :status, :created_at, :updated_at
json.url sectore_url(sectore, format: :json)
