json.extract! shipping, :id, :user_id, :title, :credit, :status, :created_at, :updated_at
json.url shipping_url(shipping, format: :json)
