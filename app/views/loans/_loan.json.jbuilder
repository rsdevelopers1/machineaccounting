json.extract! loan, :id, :user_id, :po_id, :quanty, :quotes, :freq, :int_p, :mora, :created_at, :updated_at
json.url loan_url(loan, format: :json)
