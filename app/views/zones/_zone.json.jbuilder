json.extract! zone, :id, :user_id, :description, :status, :created_at, :updated_at
json.url zone_url(zone, format: :json)
