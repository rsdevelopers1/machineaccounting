json.extract! warehouse, :id, :user_id, :description, :status, :created_at, :updated_at
json.url warehouse_url(warehouse, format: :json)
