json.extract! po, :id, :user_id, :description, :created_at, :updated_at
json.url po_url(po, format: :json)
