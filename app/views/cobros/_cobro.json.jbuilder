json.extract! cobro, :id, :cobradore_id, :pos_id, :pago_entrega, :pago_comision, :pago_prestamo, :created_at, :updated_at
json.url cobro_url(cobro, format: :json)
