json.extract! alarm, :id, :user_id, :description, :mon, :tue, :wed, :thu, :fri, :dat, :sun, :status, :created_at, :updated_at
json.url alarm_url(alarm, format: :json)
