json.extract! repair, :id, :user_id, :po_id, :description, :created_at, :updated_at
json.url repair_url(repair, format: :json)
