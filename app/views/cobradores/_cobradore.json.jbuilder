json.extract! cobradore, :id, :user_id, :name, :phone, :address, :status, :created_at, :updated_at
json.url cobradore_url(cobradore, format: :json)
