json.extract! consortium, :id, :user_id, :description, :personal, :phone, :comision_p, :status, :created_at, :updated_at
json.url consortium_url(consortium, format: :json)
