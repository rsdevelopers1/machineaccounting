json.extract! collector, :id, :user_id, :name, :phone, :address, :status, :created_at, :updated_at
json.url collector_url(collector, format: :json)
