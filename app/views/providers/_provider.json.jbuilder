json.extract! provider, :id, :user_id, :description, :status, :created_at, :updated_at
json.url provider_url(provider, format: :json)
