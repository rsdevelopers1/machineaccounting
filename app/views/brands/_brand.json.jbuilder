json.extract! brand, :id, :user_id, :description, :status, :created_at, :updated_at
json.url brand_url(brand, format: :json)
