class Shopping < ApplicationRecord
    belongs_to :shipping
    has_many :details
    belongs_to :product
    belongs_to :provider
    belongs_to :product
end
