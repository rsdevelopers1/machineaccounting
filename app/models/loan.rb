class Loan < ApplicationRecord
    belongs_to :po
    has_many :loanpays
    has_many :mora_logs

end
