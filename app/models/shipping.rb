class Shipping < ApplicationRecord
    has_many :shoppings

    accepts_nested_attributes_for :shoppings, 
    allow_destroy: true, 
    reject_if: proc { |att| att['quanty'].blank? || att['price'].blank? }
end
