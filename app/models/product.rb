class Product < ApplicationRecord
    belongs_to :brand
    belongs_to :modelo
    belongs_to :category
    has_many :shoppings
end
