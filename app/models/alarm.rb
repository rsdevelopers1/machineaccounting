class Alarm < ApplicationRecord
    has_many :alarms_config
    accepts_nested_attributes_for :alarms_config, 
    allow_destroy: true, 
    reject_if: proc { |att| att['amount'].blank? }
end
