class Consortium < ApplicationRecord
    has_many :users
    has_many :pos
    accepts_nested_attributes_for :users

end
