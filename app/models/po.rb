class Po < ApplicationRecord
    belongs_to :zone
    belongs_to :consortium
    has_many :locations
    has_many :transations
    has_many :commandlogs
    has_many :hours
    has_many :credits
    has_many :users
  
    accepts_nested_attributes_for :credits,
    allow_destroy: true,
    reject_if: proc { |att| att['credit'].blank? || att['equivale_in'].blank? || att['equivale_out'].blank? }

    accepts_nested_attributes_for :hours,
    allow_destroy: true,
    reject_if: proc { |att| att['desde'].blank? || att['hasta'].blank? }

    accepts_nested_attributes_for :users,
    allow_destroy: true,
    reject_if: proc { |att| att['username'].blank? || att['email'].blank? || att['password'].blank? || att['password_confirmation'].blank? || att['name'].blank? }

end
