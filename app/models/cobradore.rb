class Cobradore < ApplicationRecord
    has_and_belongs_to_many :zones

    has_many :users
    accepts_nested_attributes_for :users
end
