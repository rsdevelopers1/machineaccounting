class ShippingsController < ApplicationController
  before_action :set_shipping, only: [:show, :edit, :update, :destroy, :barcode, :goware]

  # GET /shippings
  # GET /shippings.json
  def index
    @shippings = Shipping.where(user_id: current_user.id).order("id DESC").paginate page: params[:page], per_page: 4
    @code = ""
    @desc = ""
  end

  def search
    if params[:code] == ""
     if params[:desc] == ""

      @shippings = Shipping.where(user_id: current_user.id).where(status: params[:status]).order("id DESC").paginate page: params[:page], per_page: 4
     else
      desc = params[:desc]
      @shippings = Shipping.where(user_id: current_user.id).where("title LIKE ?", "%#{desc}%").order("id DESC").paginate page: params[:page], per_page: 4

       
     end

    else
      @shippings = Shipping.where(user_id: current_user.id).where(id: params[:code]).order("id DESC").paginate page: params[:page], per_page: 4

    end
    
    @code = params[:code]
    @desc = params[:desc]
    render action: "index"

  end

  def barcode
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "2.pdf",
        template: "shippings/barcode.html.erb",
        layout: 'pdf'
      end
    end
  end

  def entrace
    @entraces = Shipping.where(user_id: current_user.id).where(status: 1).order("id DESC")
  end

  def goware
    @wares = Warehouse.where(user_id: current_user.id)
  end

  def modified
    respond_to do |format|
      format.html

    end

  end
  
  
  def save
    ids = params[:ids]
    warhouses = params[:warehouses_id]
    barcodes = params[:barcodes]
    references = params[:references]

    @ship = Shipping.find(params[:id])
    @ship.status = 2
    @ship.save

    a = 0
    ids.each do |id_a|

      Detail.create({shopping_id: id_a, warehouse_id: warhouses[a], barcode: barcodes[a], reference: references[a], status: 1, user_id: current_user.id})
      a = a + 1
    end
    @entraces = Shipping.where(user_id: current_user.id).where(status: 1).order("id DESC")

    render action: "entrace",notice: 'Shipping was successfully created.'


  end
  
  
  

  # GET /shippings/1
  # GET /shippings/1.json
  def show
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "2.pdf",
        template: "shippings/show.html.erb",
        layout: 'pdf'
      end
    end

  end

  # GET /shippings/new
  def new
    @shipping = Shipping.new
    @shipping.shoppings.build
  end

  # GET /shippings/1/edit
  def edit
  end

  # POST /shippings
  # POST /shippings.json
  def create
    @shipping = Shipping.new(shipping_params)
    @shipping.user_id = current_user.id
    respond_to do |format|
      if @shipping.save
        format.html { redirect_to @shipping, notice: 'Shipping was successfully created.' }
        format.json { render :show, status: :created, location: @shipping }
      else
        format.html { render :new }
        format.json { render json: @shipping.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /shippings/1
  # PATCH/PUT /shippings/1.json
  def update
    respond_to do |format|
      if @shipping.update(shipping_params)
        format.html { redirect_to @shipping, notice: 'Shipping was successfully updated.' }
        format.json { render :show, status: :ok, location: @shipping }
      else
        format.html { render :edit }
        format.json { render json: @shipping.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /shippings/1
  # DELETE /shippings/1.json
  def destroy
    @shipping.destroy
    respond_to do |format|
      format.html { redirect_to shippings_url, notice: 'Shipping was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_shipping
      @shipping = Shipping.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def shipping_params
      params.require(:shipping).permit(:user_id, :title, :credit, :status, shoppings_attributes:[:id, :product_id, :provider_id, :quanty, :status_user, :_destroy, :price, :user_id, :status])
    end
end
