class ConsortiaController < ApplicationController
  before_action :set_consortium, only: [:show, :edit, :update, :destroy]

  # GET /consortia
  # GET /consortia.json
  def index
    @consortia = Consortium.where(user_id: current_user.id)
    @code = ""
    @descripcion = ""
  end
  
  def consortia_q
    @code = params[:code]
    @descripcion = params[:name]
    @status = params[:status]
    if !@code.blank?
      @consortia = Consortium.where(user_id: current_user.id).where(id: @code)
    else

      if !@descripcion.blank?
        
        if @status == "0"
          @consortia = Consortium.where(user_id: current_user.id).where("description LIKE ?", "%#{@descripcion}%")
        else
          @consortia = Consortium.where(user_id: current_user.id).where("description LIKE ?", "%#{@descripcion}%").where(status: @status)

          
        end

      else

        if @status == "0"
          @consortia = Consortium.where(user_id: current_user.id)

        else
          @consortia = Consortium.where(user_id: current_user.id).where(status: @status)
        end
        
      end
      
    end

    render action: "index"


  end
  

  # GET /consortia/1
  # GET /consortia/1.json
  def show
  end

  # GET /consortia/new
  def new
    @consortium = Consortium.new
    @consortium.users.new

  end

  # GET /consortia/1/edit
  def edit
    
  end

  # POST /consortia
  # POST /consortia.json
  def create
    @consortium = Consortium.new(consortium_params)
    @consortium.user_id = current_user.id
    respond_to do |format|
      if @consortium.save
        format.html { redirect_to :action => :index, notice: 'Consortium was successfully created.' }
        format.json { render :show, status: :created, location: @consortium }
      else
        format.html { render :new }
        format.json { render json: @consortium.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /consortia/1
  # PATCH/PUT /consortia/1.json
  def update
    respond_to do |format|
      if @consortium.update(consortium_params)
        format.html { redirect_to :action => :index, notice: 'Consortium was successfully updated.' }
        format.json { render :show, status: :ok, location: @consortium }
      else
        format.html { render :edit }
        format.json { render json: @consortium.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /consortia/1
  # DELETE /consortia/1.json
  def destroy
    @consortium.destroy
    respond_to do |format|
      format.html { redirect_to consortia_url, notice: 'Consortium was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_consortium
      @consortium = Consortium.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def consortium_params
      params.require(:consortium).permit(:user_id, :description, :personal, :phone, :comision_p, :status, users_attributes: [:id, :name, :email, :username, :password, :password_confirmation, :level])
    end
end
