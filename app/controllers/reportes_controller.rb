class ReportesController < ApplicationController
  def ventas
    @desde = ""
    @hasta = ""
    @result = []
    @result2 = []
  end

  def ventas_q

    casino = params[:casino]
    zona   = params[:zona]
    pos    = params[:pos]
    @desde = params[:desde]
    @hasta = params[:hasta]
    data = Array.new
    @result2 = []
    @result = []

    if pos != "0"

    # @result = Transation.where(po_id: pos).where("created_at BETWEEN ? AND ?", @desde, @hasta)
    @pos_info  = Po.find(pos)
    @result =  @pos_info.transations.where("created_at BETWEEN ? AND ?", @desde, @hasta)

   else
    if casino != 0
  
    else
      
    end
    #Fin de la validacion por punto de ventas
   end

   @result.each do |r|
     if r.type_t == "bill_in"
      @result2[0]["bill_in"] += r.amount
     elsif r.type_t == "bill_out"
      @result2[0]["bill_out"] += r.amount

     else

     end 
   end
   

    render action: "ventas"

  end

  def cobros
    @desde = ""
    @hasta = ""
    @result = []
  end

  def cobros_q
    @desde = params[:desde]
    @hasta = params[:hasta]
    @zona = params[:zona]
    @pos = params[:pos]
    @ids = Po.where(user_id: current_user.id)
    if @zona == "0" && @pos == "0"      
      @result = Transation.where("po_id IN (?)",@ids.to_a).where("created_at BETWEEN ? AND ?", @desde, @hasta).where(type_t: "cobro")
    else
      if @zona != "0"
        if @pos != "0"

          #Resultados
          @result = Transation.where(po_id: @pos).where("created_at BETWEEN ? AND ?", @desde, @hasta).where(type_t: "cobro")

        else
          #Mal puesto
          @ids = Po.where(user_id: current_user.id).where(zone_id: @zona)
          @result = Transation.where("po_id IN (?)",@ids.to_a).where("created_at BETWEEN ? AND ?", @desde, @hasta).where(type_t: "cobro")

        end
      else
        
      end
    end
    render action: "cobros"
  end
  

  def comisiones
    @desde = ""
    @hasta = ""

  end

  def comisiones_q
    
    @desde = params[:desde]
    @hasta = params[:hasta]
    @zona = params[:zona]
    @pos = params[:pos]
    @ids = Po.where(user_id: current_user.id)
    
    if @zona == "0" && @pos == "0"

      @result = Transation.where("po_id IN (?)",@ids.to_a).where("created_at BETWEEN ? AND ?", @desde, @hasta).where("type_t = ? OR type_t = ?","bill_in","bill_out")
    
    else
      if @zona != "0"
        if @pos != "0"
          @result = Transation.where(po_id: @pos).where("created_at BETWEEN ? AND ?", @desde, @hasta).where(type_t: "cobro")

        else
          @ids = Po.where(user_id: current_user.id).where(zone_id: @zona)
          @result = Transation.where("po_id IN (?)",@ids.to_a).where("created_at BETWEEN ? AND ?", @desde, @hasta).where(type_t: "cobro")

        end
      else
        
      end
    end

    

    render action: "comisiones"
  end

  def estado
    @desde = ""
    @hasta = ""
    @result = []
  end

  def estado_q
    @desde = params[:desde]
    @hasta = params[:hasta]
    @pos = params[:pos]

    @result = Transation.where(po_id: @pos).where("created_at BETWEEN ? AND ?", @desde, @hasta)
    render action: "estado"
  end

  def general

  end

  def generalQ
  
    producto = params[:product]
    status = params[:status]
    data = Array.new

    if producto.blank?
      
      if status.blank?
        Shopping.where(user_id: current_user.id).group("product_id").each do |shop|
          
          data << {code: shop.id, producto: shop.product.description, marca: shop.product.brand.description, modelo: shop.product.modelo.description, categoria: shop.product.category.description, cantidad:shop.details.count}
        end

        
      else
        
      end

    else
      
    end
    render action: "general"

  end
  

  def almacen
  end

  def reparaciones
    @desde = ""
    @hasta = ""
    @result = []
  end
  
  #Metodo para repaciones
  def reparaciones_q
    @desde = params[:desde]
    @hasta = params[:hasta]
    @ids = Po.where(user_id: current_user.id)

    @result_ = RepairLog.where("po_id IN (?)",@ids.to_a).where("created_at BETWEEN ? AND ?", @desde, @hasta)

    if @result_.count > 0
      @result = @result_
    else
      @result = []
    end
    render action: "reparaciones"


  end

  def alertas
    @desde = ""
    @hasta = ""
    @result = []
  end

  def alertas_q
    @desde = params[:desde]
    @hasta = params[:hasta]
    @result = Dispatch.where(user_id: current_user.id).where("created_at BETWEEN ? AND ?", @desde, @hasta).order("id DESC") 
    render action: "alertas"

  end

  def logs
    @desde = ""
    @hasta = ""
    @result = []
    @estadistic = Commandlog.where(user_id: current_user.id)


  end

  def logsQ
    @desde = params[:desde]
    @hasta = params[:hasta]
    @result = Commandlog.where(user_id:current_user.id).where("created_at BETWEEN ? AND ?", @desde, @hasta).order("id DESC")    
    @estadistic = @result
    
    render action: "logs"

  end
  
end
