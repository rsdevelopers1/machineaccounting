class SectoresController < ApplicationController
  before_action :set_sectore, only: [:show, :edit, :update, :destroy]

  # GET /sectores
  # GET /sectores.json
  def index
    @sectores = Sectore.where(user_id: current_user.id)
  end

  # GET /sectores/1
  # GET /sectores/1.json
  def show
  end

  # GET /sectores/new
  def new
    @sectore = Sectore.new
  end

  # GET /sectores/1/edit
  def edit
  end

  # POST /sectores
  # POST /sectores.json
  def create
    @sectore = Sectore.new(sectore_params)
    @sectore.user_id = current_user.id
    respond_to do |format|
      if @sectore.save
        format.html { redirect_to :action => :index, notice: 'Sectore was successfully created.' }
        format.json { render :show, status: :created, location: @sectore }
      else
        format.html { render :new }
        format.json { render json: @sectore.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sectores/1
  # PATCH/PUT /sectores/1.json
  def update
    respond_to do |format|
      if @sectore.update(sectore_params)
        format.html { redirect_to :action => :index, notice: 'Sectore was successfully updated.' }
        format.json { render :show, status: :ok, location: @sectore }
      else
        format.html { render :edit }
        format.json { render json: @sectore.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sectores/1
  # DELETE /sectores/1.json
  def destroy
    @sectore.destroy
    respond_to do |format|
      format.html { redirect_to sectores_url, notice: 'Sectore was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sectore
      @sectore = Sectore.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sectore_params
      params.require(:sectore).permit(:user_id, :description, :status, :warehouse_id)
    end
end
