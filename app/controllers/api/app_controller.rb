include ActionView::Helpers::NumberHelper

class Api::AppController < ApplicationController

skip_before_action :authenticate_user!
before_action :start
#before_action :authorize_request, only:[:setBillIn]

def start
    #Credenciales SMS
    account_sid = 'AC5f972a1ebdb4b080da4cead1e8eb2118'
    auth_token = 'f63387037db9b7c1184659213c102a38'
    @number_prod = "+18572936431"
    @number_tester = "+18294367935"
    @client = Twilio::REST::Client.new account_sid, auth_token
end

#================================================================================>
# Apis Externas: INPUT
#================================================================================>
def setBillIn

  begin
    @last_transaction = Transation.where(po_id: params[:pos]).order("id DESC").limit(1).first
    @pos_info = Po.find(params[:pos])
    creditos = @pos_info.credits.where(credit: params[:amount])
    if creditos.count > 0
      ul_credito = creditos.last

      amount = ul_credito.equivale_in

    else
      amount = 0

    end
    porcent_c = @pos_info.comision
    bal_ant = @last_transaction.bal_end
    bal_final = bal_ant + amount
    pocent_net = porcent_c / 100.0
    comision_net = amount * pocent_net

    # Guardamos la transaccion
    Transation.create({po_id: params[:pos], type_t: "bill_in", amount: amount, comission: comision_net, bal_ant: bal_ant, bal_end: bal_final, operation_id: 0})

    render json: { c: "00", m: "Transaccion realizada"}, status: 200
  rescue => exception
    render json: { c: "17", m: "Estamos presentando problemas tecnicos"}, status: 200
  end

end

def setBillOut

  begin
    
    @last_transaction = Transation.where(po_id: params[:pos]).order("id DESC").limit(1).first
    @pos_info = Po.find(params[:pos])
    
    creditos = @pos_info.credits.where(credit: params[:amount])
    if creditos.count > 0
      ul_credito = creditos.last

      amount = ul_credito.equivalen_out

    else
      amount = 0

    end

    bal_ant = @last_transaction.bal_end
    bal_final = bal_ant - amount

    # Guardamos la transaccion
    Transation.create({po_id: params[:pos], type_t: "bill_out", amount: amount, comission: 0, bal_ant: bal_ant, bal_end: bal_final, operation_id: 0})

    render json: { c: "00", m: "Transaccion realizada"}, status: 200
  rescue => exception
    render json: { c: "17", m: "Estamos presentando problemas tecnicos"}, status: 200
  end


end

def shutdown
  @numero = ""
  begin
    @pos_info = Po.find(params[:pos])
    sim_card = @pos_info.simcard
    @deta = Detail.where(barcode: sim_card).first
    @numero = @deta.reference

    @client.api.account.messages.create(
      from: @number_prod,
      to:   @numero,
      body: 'c_off'
    )
    Commandlog.create({user_id: current_user.id, type_command:"shutdown", po_id: @pos_info.id})
    render json: { c: "00", m: "Orden enviada" }, status: 200

  rescue Twilio::REST::TwilioError => e
    ReportInternal.create({user_id: current_user.id, ex: e.to_s, number: @numero, po_id: params[:pos]})
    render json: { c: "15", m: "Estamos teniendo problema de comunicacion" }, status: 200
  rescue => exception
    render json: { c: "17", m: "Estamos presentando problemas tecnicos"}, status: 200

  end
end

def powerOn
  @numero = ""
  begin
    @pos_info = Po.find(params[:pos])
    sim_card = @pos_info.simcard
    @deta = Detail.where(barcode: sim_card).first
    @numero = @deta.reference

    @client.api.account.messages.create(
      from: @number_prod,
      to:   @numero,
      body: 'c_on'
    )
    Commandlog.create({user_id: current_user.id, type_command:"poweron", po_id: @pos_info.id})
    render json: { c: "00", m: "Orden enviada" }, status: 200

  rescue Twilio::REST::TwilioError => e
    ReportInternal.create({user_id: current_user.id, ex: e.to_s, number: @numero, po_id: params[:pos]})
    render json: { c: "15", m: "Estamos teniendo problema de comunicacion" }, status: 200
  rescue => exception
    render json: { c: "17", m: "Estamos presentando problemas tecnicos"}, status: 200

  end
end

def getLocation
  @numero = ""
  begin
    @pos_info = Po.find(params[:pos])
    sim_card = @pos_info.simcard
    @deta = Detail.where(barcode: sim_card).first
    @numero = @deta.reference

    @client.api.account.messages.create(
      from: @number_prod,
      to:   @numero,
      body: 'c_location'
    )
    Commandlog.create({user_id: current_user.id, type_command:"location", po_id: @pos_info.id})
    render json: { c: "00", m: "Orden enviada" }, status: 200

  rescue Twilio::REST::TwilioError => e
    ReportInternal.create({user_id: current_user.id, ex: e.to_s, number: @numero, po_id: params[:pos]})
    render json: { c: "15", m: "Estamos teniendo problema de comunicacion" }, status: 200
  rescue => exception
    render json: { c: "17", m: "Estamos presentando problemas tecnicos"}, status: 200

  end
end

#=================================================================================>
#Apis Externas: OUTPUT
#=================================================================================>
def setAproximidad
  begin
    @pos_info = Po.find(params[:pos])
    #Guardamos la notificacion
    @not = Notification.create({user_id: @pos_info.user_id, type_n: "aproximidad", po_id: @pos_info.id, status: 1})

    #Emitimos la notificacion
    ActionCable.server.broadcast 'web_notifications_channel', m: "Se ha detectado un acercamiento", c: @pos_info.id, user: @pos_info.user_id, t: "apprx", pv: @pos_info.description, id:@not.id

    #Respuesta
    render json: { c: "00", m: "Reporte enviado" }, status: 200

  rescue => exception
    render json: { c: "17", m: "Estamos teniendo errores internos" }, status: 200

  end
  
end

def setShutDown
  begin
    @pos_info = Po.find(params[:pos])
    #Guardamos la notificacion
    @not = Notification.create({user_id: @pos_info.user_id, type_n: "apagado", po_id: @pos_info.id, status: 1})

    #Emitimos la notificacion
    ActionCable.server.broadcast 'web_notifications_channel', m: "Pos ha sido apagado", c: @pos_info.id, user: @pos_info.user_id, t: "apagado", pv: @pos_info.description, id:@not.id

    #Modificamos el estado
    @pos_info.status = 2
    @pos_info.save

    #Respuesta
    render json: { c: "00", m: "Reporte enviado" }, status: 200

  rescue => exception
    render json: { c: "17", m: "Estamos teniendo errores internos" }, status: 200

  end
  
end

def setPoweOn
  begin
    @pos_info = Po.find(params[:pos])
    #Guardamos la notificacion
    @not = Notification.create({user_id: @pos_info.user_id, type_n: "encendido", po_id: @pos_info.id, status: 1})

    #Emitimos la notificacion
    ActionCable.server.broadcast 'web_notifications_channel', m: "Pos ha sido encendido", c: @pos_info.id, user: @pos_info.user_id, t: "encendido", pv: @pos_info.description, id:@not.id

    #Modificamos el estado
    @pos_info.status = 1
    @pos_info.save

    #Respuesta
    render json: { c: "00", m: "Reporte enviado" }, status: 200

  rescue => exception
    render json: { c: "17", m: "Estamos teniendo errores internos" }, status: 200

  end

end

def setWifiOn
  begin
    @pos_info = Po.find(params[:pos])
    #Guardamos la notificacion
    @not = Notification.create({user_id: @pos_info.user_id, type_n: "wifi_on", po_id: @pos_info.id, status: 1})

    #Emitimos la notificacion
    ActionCable.server.broadcast 'web_notifications_channel', m: "Pos conectado a "+ @pos_info.wifi_ssd, c: @pos_info.id, user: @pos_info.user_id, t: "wifi_on", pv: @pos_info.description, id:@not.id

    #Modificamos el estado
    @pos_info.wifi_status = 1
    @pos_info.save

    #Respuesta
    render json: { c: "00", m: "Reporte enviado" }, status: 200

  rescue => exception
    render json: { c: "17", m: "Estamos teniendo errores internos" }, status: 200

  end

end

def setWifiOff
  begin
    @pos_info = Po.find(params[:pos])
    #Guardamos la notificacion
    @not = Notification.create({user_id: @pos_info.user_id, type_n: "wifi_off", po_id: @pos_info.id, status: 1})

    #Emitimos la notificacion
    ActionCable.server.broadcast 'web_notifications_channel', m: "Pos desconectado de "+@pos_info.wifi_ssd, c: @pos_info.id, user: @pos_info.user_id, t: "wifi_off", pv: @pos_info.description, id:@not.id

    #Modificamos el estado
    @pos_info.wifi_status = 0
    @pos_info.save

    #Respuesta
    render json: { c: "00", m: "Reporte enviado" }, status: 200

  rescue => exception
    render json: { c: "17", m: "Estamos teniendo errores internos" }, status: 200

  end

end


def setDataOn
  begin
    @pos_info = Po.find(params[:pos])
    #Guardamos la notificacion
    @not = Notification.create({user_id: @pos_info.user_id, type_n: "data_on", po_id: @pos_info.id, status: 1})

    #Emitimos la notificacion
    ActionCable.server.broadcast 'web_notifications_channel', m: "Pos conectado por datos", c: @pos_info.id, user: @pos_info.user_id, t: "data_on", pv: @pos_info.description, id:@not.id

    #Modificamos el estado
    @pos_info.data_status = 1
    @pos_info.save

    #Respuesta
    render json: { c: "00", m: "Reporte enviado" }, status: 200

  rescue => exception
    render json: { c: "17", m: "Estamos teniendo errores internos" }, status: 200

  end

end


def setDataOff
  begin
    @pos_info = Po.find(params[:pos])
    #Guardamos la notificacion
    @not = Notification.create({user_id: @pos_info.user_id, type_n: "data_off", po_id: @pos_info.id, status: 1})

    #Emitimos la notificacion
    ActionCable.server.broadcast 'web_notifications_channel', m: "Pos desconectado de la red movil", c: @pos_info.id, user: @pos_info.user_id, t: "data_off", pv: @pos_info.description, id:@not.id

    #Modificamos el estado
    @pos_info.data_status = 0
    @pos_info.save

    #Respuesta
    render json: { c: "00", m: "Reporte enviado" }, status: 200

  rescue => exception
    render json: { c: "17", m: "Estamos teniendo errores internos" }, status: 200

  end

end


def setLocations
  begin
    @pos_info = Po.find(params[:pos])
    #Guardamos la notificacion
    @not = Notification.create({user_id: @pos_info.user_id, type_n: "location", po_id: @pos_info.id, status: 1})

    #Emitimos la notificacion
    ActionCable.server.broadcast 'web_notifications_channel', m: "GPS reporte", c: @pos_info.id, user: @pos_info.user_id, t: "location", pv: @pos_info.description, longitud: params[:lon], latitud: params[:lat], id:@not.id

    #Modificamos el estado
    @pos_info.locations.create({longitud: params[:lon], latitud: params[:lat]})


    #Respuesta
    render json: { c: "00", m: "Reporte enviado" }, status: 200

  rescue => exception
    render json: { c: "17", m: "Estamos teniendo errores internos" }, status: 200

  end

end

#=================================================================================>
# TEST AREA
#=================================================================================>

#Apagar
def POWEROFF_TEST
    @client.api.account.messages.create(
        from: @number_prod,
        to:   @number_tester,
        body: 'p_off'
      )

      #Respuesta
      render json: { c: "00", m: "Orden enviada" }, status: 200

end

#Encender
def POWERON_TEST

    @client.api.account.messages.create(
        from: @number_prod,
        to:   @number_tester,
        body: 'p_on'
      )

      render json: { c: "00", m: "Orden enviada" }, status: 200

    
end

#=================================================================================>
# Apis interna
#=================================================================================>
# Metodo para traer todas las posiciones de cada POS
def getDirections

  begin
      #Variables globales
    @zone   = params[:zone]
    @status = params[:status]
    data = Array.new

    if @zone == "0" && @status == "0"
      puts "Correto"
      # Busqueda de inicio 
      @puntos = Po.where(user_id: current_user.id)

    else
      #Busqueda por filtro
      if @zone == "0"
      
        if @status == "0"
        
          @puntos = Po.where(user_id: current_user.id)

        else

          @puntos = Po.where(user_id: current_user.id).where(status: @status)

        end
      else
         
        if @status == "0"
          
            @puntos = Po.where(user_id: current_user.id).where(zone_id: @zone)
  
          else
            puts "Correto"
  
            @puntos = Po.where(user_id: current_user.id).where(zone_id: @zone).where(status: @status)
  
          end

        end

    end

    @puntos.each do |punto| 
      localizacion = punto.locations.last
      transaccion = punto.transations.last
      datos = {description: punto.description, lon: localizacion.longitud, lat: localizacion.latitud, balance: number_to_currency(transaccion.bal_end)}
      data << datos

    end

    render json: { c: "00", m: "Consulta exitosa", data: data }, status: 200

  rescue => exception
    render json: { c: "17", m: "Estamos teniendo problemas internos" }, status: 200

  end
  
end

#Metodo para traer todo los datos para los controles
def getControl
  begin

      #Session del usuario
  user_now = current_user.id 
  #varables
  @point    = Po.where(id: params[:pos]).where(user_id: current_user.id).where("status = ? OR status = ?", 1, 2).limit(1).first
  @location = Location.where(po_id: params[:pos]).order("id DESC").limit(1).first
  @transac = Transation.where(po_id: params[:pos]).order("id DESC").limit(1).first
  data = Array.new
  if @point.user_id == user_now
    datos = {id: @point.id, status_: @point.status, longi: @location.longitud, lati: @location.latitud, last: @transac.created_at}
    data << datos
    render json: { c: "00", m: "POS encontrado",  data: data}, status: 200

  else
    # Ojo intento de acceso no permitido
    render json: { c: "25", m: "POS no pertenece a esta cuenta" }, status: 200

  end

  rescue NoMethodError => e 
    render json: { c: "100", m: "POS no encotrado" }, status: 200

  rescue ActiveRecord::RecordNotFound
    render json: { c: "100", m: "POS no encotrado" }, status: 200

  end
  
end

# Metodo para buscar entrada
def getEntrance

    @q    = params[:q_for]
    @ref  = params[:ref]
    data = Array.new
    if @q == "barcode"

      @res = Detail.where(barcode: @ref).last

    elsif @q == "reference"
      @res = Detail.where(reference: @ref).last
    end

    if !@res.nil?

      # Entrada Encontrada
      user_p = @res.shopping.shipping.user_id
      if current_user.id != user_p
        render json: { c: "05", m: "Compra no pertenece a este usuario" }, status: 200
        
      else
        datos = {id: @res.id, barcode: @res.barcode, ref: @res.reference}
        data << datos
            # Respuesta
        render json: { c: "00", m: "Consulta realizada con exito", data: data }, status: 200
      end

    else

      #Entrada no encontrada
      render json: { c: "404", m: "Registro no encontrado" }, status: 200

    end




end

# Metodo para realizar cambios en una entrada
def changeEntrace
  @detail = Detail.find(params[:id])
  @detail.barcode = params[:barcode]
  @detail.reference = params[:ref]

  if @detail.save
    render json: { c: "00", m: "Datos actualizados con exito" }, status: 200

  else
    render json: { c: "17", m: "Estamos teniendo errores internos" }, status: 200

  end
end

# Metodo para velidar dispositivo
def checkDevice
  begin
    @res = Detail.where(barcode: params[:barcode]).last
    if !@res.nil?
     tipo =  @res.shopping.product.type_p

     if params[:t] == tipo.to_s
      render json: { c: "00", m: "Barcode validado" }, status: 200
     else
      render json: { c: "05", m: "Tipo de producto no valido" }, status: 200

     end

    else
            render json: { c: "404", m: "Registro no encontrado" }, status: 200
    end

    
  rescue => exception
    render json: { c: "17", m: "Estamos teniendo errores internos" }, status: 200

  end
end

# Metodo para traer las zonas de un cobrador
def getCollectorZone
  begin
    @colletor = Cobradore.find(params[:c_id])
    data = Array.new
    @colletor.zones.each do |zone|
      data << {id: zone.id, description: zone.description}
    end

    render json: { c: "00", m: "Consulta realizada con exito", data: data }, status: 200


  rescue => exception
    render json: { c: "17", m: "Estamos teniendo errores internos" }, status: 200

  end
end

# Metodo para traer los POS por zonas
def getPOSZone
  begin
    @zone = Zone.find(params[:z_id])
    data = Array.new
    @zone.pos.each do |pv|
      data << {id: pv.id, description: pv.description}
    end

    render json: { c: "00", m: "Consulta realizada con exito", data: data }, status: 200

  rescue => exception
    render json: { c: "17", m: "Estamos teniendo errores internos" }, status: 200

  end
end

# Metodo para traer zonas
def getZones
  begin
    @zone = Zone.where(user_id: current_user.id)
    data = Array.new
    @zone.each do |pv|
      data << {id: pv.id, description: pv.description}
    end

    render json: { c: "00", m: "Consulta realizada con exito", data: data }, status: 200

  rescue => exception
    render json: { c: "17", m: "Estamos teniendo errores internos" }, status: 200

  end
end

# Metodo para traer zonas
def getCasinos
  begin
    @casinos = Consortium.where(user_id: current_user.id)
    data = Array.new
    @casinos.each do |pv|
      data << {id: pv.id, description: pv.description}
    end

    render json: { c: "00", m: "Consulta realizada con exito", data: data }, status: 200

  rescue => exception
    render json: { c: "17", m: "Estamos teniendo errores internos" }, status: 200

  end
end

def getDebt
  begin

    @pos_info = Po.find(params[:pos])
    @last_transaction = Transation.where(po_id: params[:pos]).last

    balance = @last_transaction.bal_end
    porcent_c = @pos_info.comision
    pocent_net = porcent_c / 100.0

    comision_final = balance * pocent_net
    deuda = balance - comision_final
    render json: { c: "00", m: "Consulta exitosa", balance: balance, deuda: deuda, comision: comision_final }, status: 200
    
  rescue => exception
    render json: { c: "17", m: "Estamos teniendo errores internos" }, status: 200

  end
end

#Metodo para traer zonas por cobradores
def getZoneByCobrador
  begin
    data = Array.new
    @cobrador = Cobradore.find(params[:cobrador])
    @cobrador.zones.each do |personal|
      data << {id: personal.id, name: personal.description }
      
    end

    render json: { c: "00", m: "Consulta exitosa", data: data }, status: 200
  rescue => exception
    render json: { c: "17", m: "Estamos teniendo errores internos" }, status: 200

  end
end

def getConfigByNumber
  
    detalles = Detail.where(reference: params[:number])
    if detalles.count > 0
      barcode = detalles.last.barcode
      pos = Po.where(simcard: barcode).last
      @number = "+18294367935"
      token = JsonWebToken.encode(number: @number)
      puts token
      @configuration = "c_c_d_"+pos.connect_default.to_s+"_ws_"+pos.wifi_ssd.to_s+"_wp_"+pos.wifi_password.to_s+"_h_www.a.org_mt_"+pos.bill_type.to_s+"_id_"+pos.id.to_s+"_t_"+token
      @client.api.account.messages.create(
        from: @number_prod,
        to:   @number,
        body: @configuration
      )
      render json: { c: "00", m: "Consulta exitosa"}, status: 200

    else
      render json: { c: "05", m: "Referencia no encontrada" }, status: 200

    end
    
 

end

#=================================================================================>
# Crons
#=================================================================================>
def cron_alarms
  begin
    @alertas = Alarm.where(status: 1)
    @alertas.each do |alerta|
      hoy = Date.today.strftime("%A").downcase
      aproved = false

      if hoy == "sunday" && alerta.sun == "1"
        aproved = true

      elsif hoy == "monday" && alerta.mon == "1"
        aproved = true

      elsif hoy == "tuesday" && alerta.tue == "1"
        aproved = true

      elsif hoy == "wednesday" && alerta.wed == "1"
        aproved = true

      elsif hoy == "thursday" && alerta.thu == "1"
        aproved = true
      elsif hoy == "friday" && alerta.fri == "1"
        aproved = true

      elsif hoy == "saturday" && alerta.sat== "1"
        aproved = true
      end
      
      
      if aproved == true

        fecha_q = Time.zone.now.beginning_of_day.to_s.split(" ")

      alerta.alarms_config.each do |config|
        ultima_transacion = Transation.where("created_at BETWEEN ? AND ?", fecha_q[0], fecha_q[0]).where(type_t: "bill_in").where(po_id: config.po.id).sum(:amount)
        if config.alarm_type == "1" && config.status == 1
            #Alarma de venta mayor que:
            if ultima_transacion > config.amount
              #Disparar alerta
              Dispatch.create({user_id: config.po.user_id, po_id: config.po.id, alarm_id: alerta.id, balance: ultima_transacion, 	alarms_config_id: config.id})
              
              #Notificar

              # Enviame el correo electronico

            end 
        elsif config.alarm_type == "2" && config.status == 1
          #Alarma de venta menor que:
          if ultima_transacion < config.amount
            #Disparar alerta
            Dispatch.create({user_id: config.po.user_id, po_id: config.po.id, alarm_id: alerta.id, balance: ultima_transacion, 	alarms_config_id: config.id})
            #Notificar
            
            # Enviame el correo electronico
            #Traduciendo el plazo
          end 
        end
        

      end

    end
    end

    render json: { c: "00", m: "Consulta exitosa"}, status: 200

  rescue => exception
    render json: { c: "17", m: "Estamos teniendo errores internos" }, status: 200

  end
 
end

def cron_hours
  begin
    #Dia de hoy
    hoy = Date.today.strftime("%A").downcase

    #Buscando los puntos de ventas donde sean automatizalos
    pos = Po.where(hours: 1)

    pos.each do |po|
      hora = po.hours.where(day: hoy).last
      hora_desde = DateTime.parse(hora.desde)
      hora_hasta = DateTime.parse(hora.hasta)
      hora_now = DateTime.now

      numeros = Detail.where(barcode: po.simcard).last
      @numero = numeros.reference

      if hora_now > hora_desde && po.status == 2
        # La orden es apagar la maquina

      @client.api.account.messages.create(
        from: @number_prod,
        to:   @numero,
        body: 'c_off'
      )
      elsif hora_now > hora_hasta && po.status == 1
        # La orden es apagar la maquina por sobre tiempo
        @client.api.account.messages.create(
          from: @number_prod,
          to:   @numero,
          body: 'c_off'
        )

      end



    end
    render json: { c: "00", m: "Consulta exitosa"}, status: 200

  rescue => exception

    render json: { c: "17", m: "Estamos teniendo errores internos" }, status: 200
  
  end
end

def cron_moras

  begin

    prestamos = Loan.where("status = ? OR status = ?", 1, 3)
    prestamos.each do |p|
      #Buscando prestamos

      ultimo_pago = p.loanpays.last.created_at.to_date
      hora_now = DateTime.now
      dias = (hora_now - ultimo_pago).to_i
      moroso = false
      #Verificando
      if p.freq == "1"
        #Por dias
        if dias > 0
          moroso = true

        end
      elsif p.freq == "2"
        if dias > 7
          moroso = true
        end

      elsif p.freq == "3"
        if dias > 15
          moroso = true
        end

      elsif p.freq == "4"
        if dias > 30
          moroso = true          
        end
      end

      #Calculando y Registrando
      cantidad_prestada = p.quanty
      per_mora = p.mora/100.0
      
      total_mora = cantidad_prestada * per_mora
      sum_mora = p.mora_acomulada + total_mora

      if moroso == true 
        #El prestamo genero mora 
        p.mora_acomulada = sum_mora
        p.save

        #Registrando en el log
        MoraLog.create({loan_id: p.id, amount: total_mora, status: 1})

      end

    end
    render json: { c: "00", m: "Consulta exitosa"}, status: 200
  rescue => exception
    render json: { c: "17", m: "Estamos teniendo errores internos" }, status: 200
  end

end

#=================================================================================>
#                                 Estadisticas
#=================================================================================>
# Ventas Diaria
#=================================================================================>
def estVentasDiaria

end
#=================================================================================>
# Ventas Semanales
#=================================================================================>
def estVentasSemanal

end
#=================================================================================>
# Puntos de Ventas con mas ventas
#=================================================================================>
def topPOS

end
#=================================================================================>

# Fin de la clase
end