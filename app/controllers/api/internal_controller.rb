class Api::InternalController < ApplicationController


# metodo para verificar que Barcode introducido
# es un dispositivo valido
def checkDevice
    render json: { c: "00", m: "Orden enviada" }, status: 200
 
end


# Metodo para traer los prestamos pedientes
def getPrestamosPendientes
    pos = params[:pos]
    @puntos = Prestamo.where(po_id: pos)
    @response = []

   
    render json: { c: "00", m: "Orden enviada" }, status: 200


end

# Metodo para traer los balances de llos puntos de ventas
def getPosBalances
       render json: { c: "00", m: "Orden enviada" }, status: 200
 
end


end