class NotifyController < ApplicationController
  def list
    @desde = ""
    @hasta = ""
    @result = Notification.where(user_id: current_user.id).order("id DESC")
  end

  def notify_q
    @desde = params[:desde]
    @hasta = params[:hasta]
    status = params[:status]

    if status == "0"
      @result = Notification.where(user_id:current_user.id).where("created_at BETWEEN ? AND ?", @desde, @hasta).order("id DESC") 

    elsif status == "1"
      @result = Notification.where(user_id:current_user.id).where(status: 1).where("created_at BETWEEN ? AND ?", @desde, @hasta).order("id DESC") 

    else
      @result =  Notification.where(user_id:current_user.id).where(status: 2).where("created_at BETWEEN ? AND ?", @desde, @hasta).order("id DESC") 

    end

    render action: "list"

  end
  

  def show
  end
end
