class LoansController < ApplicationController
  before_action :set_loan, only: [:show, :edit, :update, :destroy]

  # GET /loans
  # GET /loans.json
  def index
    @loans = Loan.where(user_id: current_user.id)
    @code = ""
  end

  def loan_q
    @code  = params[:code]
    status = params[:status]
    if !@code.blank?
      @loans = Loan.where(user_id: current_user.id).where(id: @code)
    else
      if status == "0"
        @loans = Loan.where(user_id: current_user.id)
      else
        @loans = Loan.where(user_id: current_user.id).where(status: status)
      end

    end

    render action: "index"
  end
  

  # GET /loans/1
  # GET /loans/1.json
  def show
  end

  # GET /loans/new
  def new
    @loan = Loan.new
  end

  # GET /loans/1/edit
  def edit
  end

  # POST /loans
  # POST /loans.json
  def create
    @loan = Loan.new(loan_params)
    @loan.user_id = current_user.id
    @loan.mora_acomulada = 0
    @loan.status = 1
    respond_to do |format|
      if @loan.save
        Loanpay.create({loan_id: @loan.id, amount: 0, amount_mora: 0, balance: @loan.quanty, operation_id: 0})
        
        format.html { redirect_to @loan, notice: 'Loan was successfully created.' }
        format.json { render :show, status: :created, location: @loan }
      else
        format.html { render :new }
        format.json { render json: @loan.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /loans/1
  # PATCH/PUT /loans/1.json
  def update
    respond_to do |format|
      if @loan.update(loan_params)
        format.html { redirect_to @loan, notice: 'Loan was successfully updated.' }
        format.json { render :show, status: :ok, location: @loan }
      else
        format.html { render :edit }
        format.json { render json: @loan.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /loans/1
  # DELETE /loans/1.json
  def destroy
    @loan.destroy
    respond_to do |format|
      format.html { redirect_to loans_url, notice: 'Loan was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_loan
      @loan = Loan.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def loan_params
      params.require(:loan).permit(:user_id, :po_id, :quanty, :quotes, :freq, :int_p, :mora, :pays)
    end
end
