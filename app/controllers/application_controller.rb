class ApplicationController < ActionController::Base
    before_action :authenticate_user!

    def authorize_request
        header = request.headers['Authorization']
        header = header.split(' ').last if header
    
        begin
          @decoded = JsonWebToken.decode(header)
          @current_user2 = Detail.where(reference: @decoded[:number])
        rescue ActiveRecord::RecordNotFound => e
            render json: { c: "10", m: "Dispositivo no encontrado"}, status: :unauthorized
        rescue JWT::DecodeError => e
          render json: { c: "10", m: "Dispositivo no autorizado"}, status: :unauthorized
        end
      end
    
      def not_found
        render json: { error: 'not_found' }
      end
end
