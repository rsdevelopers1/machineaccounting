class EstadisticsController < ApplicationController
  def welcome
    @data_transational = Transation.group(:type_t).group_by_day_of_week(:created_at, format: "%a").sum(:amount)
  end
end
