class CobradoresController < ApplicationController
  before_action :set_cobradore, only: [:show, :edit, :update, :destroy]

  # GET /cobradores
  # GET /cobradores.json
  def index
    @cobradores = Cobradore.where(user_id: current_user.id)
  end

  # GET /cobradores/1
  # GET /cobradores/1.json
  def show
  end

  # GET /cobradores/new
  def new
    @cobradore = Cobradore.new
    @cobradore.users.new

  end

  # GET /cobradores/1/edit
  def edit
  end

  # POST /cobradores
  # POST /cobradores.json
  def create
    @cobradore = Cobradore.new(cobradore_params)
    @cobradore.user_id = current_user.id
 
    respond_to do |format|
      if @cobradore.save
        format.html { redirect_to :action => :index, notice: 'Cobradore was successfully created.' }
        format.json { render :show, status: :created, location: @cobradore }
      else
        format.html { render :new }
        format.json { render json: @cobradore.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cobradores/1
  # PATCH/PUT /cobradores/1.json
  def update
    respond_to do |format|
      if @cobradore.update(cobradore_params)
        format.html { redirect_to :action => :index, notice: 'Cobradore was successfully updated.' }
        format.json { render :show, status: :ok, location: @cobradore }
      else
        format.html { render :edit }
        format.json { render json: @cobradore.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cobradores/1
  # DELETE /cobradores/1.json
  def destroy
    @cobradore.destroy
    respond_to do |format|
      format.html { redirect_to cobradores_url, notice: 'Cobradore was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cobradore
      @cobradore = Cobradore.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cobradore_params
      params.require(:cobradore).permit(:user_id, :name, :phone, :address, :status, zone_ids:[], users_attributes: [:id, :name, :email, :username, :password, :password_confirmation, :level])
    end
end
