class RepairsController < ApplicationController
  before_action :set_repair, only: [:show, :edit, :destroy]

  # GET /repairs
  # GET /repairs.json
  def index
    @repairs = Repair.where(user_id: current_user.id)
  end

  # GET /repairs/1
  # GET /repairs/1.json
  def show
  end

  # GET /repairs/new
  def new
    @repair = Repair.new
  end

  # GET /repairs/1/edit
  def edit
  end

  # POST /repairs
  # POST /repairs.json
  def create
    @repair = Repair.new(repair_params)
    @repair.user_id = current_user.id
    respond_to do |format|
      if @repair.save
        
        @pos = Po.find(@repair.po_id)
        @pos.status = 3
        @pos.save 

        RepairLog.create({repair_id: @repair.id, comment: @repair.description, po_id: @repair.po_id})

        format.html { redirect_to repairs_url, notice: 'Repair was successfully created.' }
        format.json { render :show, status: :created, location: @repair }
      else
        format.html { render :new }
        format.json { render json: @repair.errors, status: :unprocessable_entity }
      end
    end
  end



  # DELETE /repairs/1
  # DELETE /repairs/1.json
  def destroy
    @pos = Po.find(@repair.po_id)
    @pos.status = 1
    @pos.save 

    @repair.destroy
    respond_to do |format|
      format.html { redirect_to repairs_url, notice: 'Repair was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_repair
      @repair = Repair.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def repair_params
      params.require(:repair).permit(:user_id, :po_id, :description)
    end
end
