class AddWifiStatusToPos < ActiveRecord::Migration[5.2]
  def change
    add_column :pos, :wifi_status, :integer
    add_column :pos, :data_status, :integer

  end
end
