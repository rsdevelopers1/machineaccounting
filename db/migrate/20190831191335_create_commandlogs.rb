class CreateCommandlogs < ActiveRecord::Migration[5.2]
  def change
    create_table :commandlogs do |t|
      t.integer :user_id
      t.string :type_command

      t.timestamps
    end
  end
end
