class CreateLoanpays < ActiveRecord::Migration[5.2]
  def change
    create_table :loanpays do |t|
      t.integer :loan_id
      t.float :amount
      t.float :amount_mora
      t.float :balance
      t.integer :operation_id

      t.timestamps
    end
  end
end
