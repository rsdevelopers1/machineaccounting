class AddConsortiumIdToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :consortium_id, :integer
  end
end
