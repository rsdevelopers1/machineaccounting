class AddConsotiumIdToPos < ActiveRecord::Migration[5.2]
  def change
    add_column :pos, :consortium_id, :integer
  end
end
