class CreatePrestamosPagos < ActiveRecord::Migration[5.2]
  def change
    create_table :prestamos_pagos do |t|
      t.string :prestamo_id
      t.float :amount
      t.float :balance
      t.integer :cobro_id

      t.timestamps
    end
  end
end
