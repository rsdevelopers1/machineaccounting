class AddMoraAcomuladaToLoans < ActiveRecord::Migration[5.2]
  def change
    add_column :loans, :mora_acomulada, :float
  end
end
