class CreateMoraLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :mora_logs do |t|
      t.integer :loan_id
      t.float :amount
      t.integer :status

      t.timestamps
    end
  end
end
