class AddPaysToLoans < ActiveRecord::Migration[5.2]
  def change
    add_column :loans, :pays, :integer
  end
end
