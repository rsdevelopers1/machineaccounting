class CreatePrestamos < ActiveRecord::Migration[5.2]
  def change
    create_table :prestamos do |t|
      t.string :concepto
      t.integer :post_id
      t.integer :origin_id
      t.float :amount

      t.timestamps
    end
  end
end
