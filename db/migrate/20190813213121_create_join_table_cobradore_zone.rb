class CreateJoinTableCobradoreZone < ActiveRecord::Migration[5.2]

  def change
    create_join_table :cobradores, :zones do |t|
      # t.index [:cobradore_id, :zone_id]
      # t.index [:zone_id, :cobradore_id]
    end
  end

end
