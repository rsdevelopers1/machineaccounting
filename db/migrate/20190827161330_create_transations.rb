class CreateTransations < ActiveRecord::Migration[5.2]
  def change
    create_table :transations do |t|
      t.integer :pos_id
      t.string :type_t
      t.float :amount
      t.float :comission
      t.float :bal_ant
      t.float :bal_end
      t.integer :operation_id

      t.timestamps
    end
  end
end
