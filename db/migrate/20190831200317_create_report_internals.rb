class CreateReportInternals < ActiveRecord::Migration[5.2]
  def change
    create_table :report_internals do |t|
      t.integer :user_id
      t.string :ex
      t.string :number
      t.integer :po_id

      t.timestamps
    end
  end
end
