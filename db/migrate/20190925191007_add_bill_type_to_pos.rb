class AddBillTypeToPos < ActiveRecord::Migration[5.2]
  def change
    add_column :pos, :bill_type, :integer
  end
end
