class CreateLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :locations do |t|
      t.integer :po_id
      t.string :longitud
      t.string :latitud

      t.timestamps
    end
  end
end
