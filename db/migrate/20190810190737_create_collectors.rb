class CreateCollectors < ActiveRecord::Migration[5.2]
  def change
    create_table :collectors do |t|
      t.integer :user_id
      t.string :name
      t.string :phone
      t.string :address
      t.integer :status

      t.timestamps
    end
  end
end
