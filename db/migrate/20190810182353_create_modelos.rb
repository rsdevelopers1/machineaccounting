class CreateModelos < ActiveRecord::Migration[5.2]
  def change
    create_table :modelos do |t|
      t.integer :user_id
      t.integer :brand_id
      t.string :description
      t.integer :status

      t.timestamps
    end
  end
end
