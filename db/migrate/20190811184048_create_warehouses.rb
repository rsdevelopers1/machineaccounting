class CreateWarehouses < ActiveRecord::Migration[5.2]
  def change
    create_table :warehouses do |t|
      t.integer :user_id
      t.string :description
      t.integer :status

      t.timestamps
    end
  end
end
