class AddWifiSsdToPos < ActiveRecord::Migration[5.2]
  def change
    add_column :pos, :wifi_ssd, :string
    add_column :pos, :wifi_password, :string

  end
end
