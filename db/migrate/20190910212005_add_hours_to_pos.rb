class AddHoursToPos < ActiveRecord::Migration[5.2]
  def change
    add_column :pos, :hours, :int
  end
end
