class AddPriceToShoppings < ActiveRecord::Migration[5.2]
  def change
    add_column :shoppings, :price, :float
  end
end
