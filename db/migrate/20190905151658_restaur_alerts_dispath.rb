class RestaurAlertsDispath < ActiveRecord::Migration[5.2]
  def change
    rename_column :dispatches, :alert_id, :alarm_id

  end
end
