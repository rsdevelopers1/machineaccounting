class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.integer :user_id
      t.integer :type_p
      t.integer :brand_id
      t.integer :modelo_id
      t.integer :category_id

      t.timestamps
    end
  end
end
