class AddConnectDefaultToPos < ActiveRecord::Migration[5.2]
  def change
    add_column :pos, :connect_default, :integer
  end
end
