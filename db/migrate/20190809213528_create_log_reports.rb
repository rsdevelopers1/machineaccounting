class CreateLogReports < ActiveRecord::Migration[5.2]
  def change
    create_table :log_reports do |t|
      t.string :code_m
      t.string :type_report
      t.float :balance
      t.integer :amount
      t.string :level

      t.timestamps
    end
  end
end
