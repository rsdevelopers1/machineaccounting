class CreateAlarmsConfigs < ActiveRecord::Migration[5.2]
  def change
    create_table :alarms_configs do |t|
      t.integer :alarm_id
      t.integer :po_id
      t.string :alarm_type
      t.float :amount
      t.integer :status

      t.timestamps
    end
  end
end
