class CreateBalances < ActiveRecord::Migration[5.2]
  def change
    create_table :balances do |t|
      t.string :code_m
      t.float :balance_ant
      t.float :balance_final
      t.string :operation_type
      t.integer :operation_id

      t.timestamps
    end
  end
end
