class CreateRepairs < ActiveRecord::Migration[5.2]
  def change
    create_table :repairs do |t|
      t.integer :user_id
      t.integer :po_id
      t.string :description

      t.timestamps
    end
  end
end
