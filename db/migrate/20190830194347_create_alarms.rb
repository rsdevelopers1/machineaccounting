class CreateAlarms < ActiveRecord::Migration[5.2]
  def change
    create_table :alarms do |t|
      t.integer :user_id
      t.string :description
      t.string :mon
      t.string :tue
      t.string :wed
      t.string :thu
      t.string :fri
      t.string :dat
      t.string :sun
      t.integer :status

      t.timestamps
    end
  end
end
