class CreateRepairLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :repair_logs do |t|
      t.integer :repair_id
      t.string :comment

      t.timestamps
    end
  end
end
