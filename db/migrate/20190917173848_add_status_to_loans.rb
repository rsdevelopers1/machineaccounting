class AddStatusToLoans < ActiveRecord::Migration[5.2]
  def change
    add_column :loans, :status, :integer
  end
end
