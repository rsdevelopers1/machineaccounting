class CreateShoppings < ActiveRecord::Migration[5.2]
  def change
    create_table :shoppings do |t|
      t.integer :shipping_id
      t.integer :product_id
      t.integer :quanty
      t.integer :provider_id
      t.integer :warehouse_id
      t.integer :status_user
      t.string :barcode
      t.string :ref
      t.integer :status

      t.timestamps
    end
  end
end
