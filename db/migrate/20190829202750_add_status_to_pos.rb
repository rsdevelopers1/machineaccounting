class AddStatusToPos < ActiveRecord::Migration[5.2]
  def change
    add_column :pos, :status, :integer
  end
end
