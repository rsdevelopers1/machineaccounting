class CreateAlerts < ActiveRecord::Migration[5.2]
  def change
    create_table :alerts do |t|
      t.string :code_m
      t.string :type_alert
      t.integer :status

      t.timestamps
    end
  end
end
