class AddPriceToShippings < ActiveRecord::Migration[5.2]
  def change
    add_column :shippings, :price, :float
  end
end
