class CreateCobros < ActiveRecord::Migration[5.2]
  def change
    create_table :cobros do |t|
      t.integer :cobradore_id
      t.integer :pos_id
      t.float :pago_entrega
      t.float :pago_comision
      t.string :pago_prestamo

      t.timestamps
    end
  end
end
