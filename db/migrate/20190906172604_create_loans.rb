class CreateLoans < ActiveRecord::Migration[5.2]
  def change
    create_table :loans do |t|
      t.integer :user_id
      t.integer :po_id
      t.float :quanty
      t.integer :quotes
      t.string :freq
      t.integer :int_p
      t.integer :mora

      t.timestamps
    end
  end
end
