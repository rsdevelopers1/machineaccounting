class AddWarehouseIdToSectores < ActiveRecord::Migration[5.2]
  def change
    add_column :sectores, :warehouse_id, :integer
  end
end
