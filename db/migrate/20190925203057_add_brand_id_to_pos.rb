class AddBrandIdToPos < ActiveRecord::Migration[5.2]
  def change
    add_column :pos, :brand_id, :integer
    add_column :pos, :modelo_id, :integer
  end
end
