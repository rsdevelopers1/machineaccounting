class AddPoIdToCommandLogs < ActiveRecord::Migration[5.2]
  def change
    add_column :commandlogs, :po_id, :integer
  end
end
