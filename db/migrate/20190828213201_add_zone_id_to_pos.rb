class AddZoneIdToPos < ActiveRecord::Migration[5.2]
  def change
    add_column :pos, :zone_id, :integer
  end
end
