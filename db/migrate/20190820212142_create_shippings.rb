class CreateShippings < ActiveRecord::Migration[5.2]
  def change
    create_table :shippings do |t|
      t.integer :user_id
      t.string :title
      t.integer :credit
      t.integer :status

      t.timestamps
    end
  end
end
