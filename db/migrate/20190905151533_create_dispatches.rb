class CreateDispatches < ActiveRecord::Migration[5.2]
  def change
    create_table :dispatches do |t|
      t.integer :user_id
      t.integer :po_id
      t.integer :alert_id
      t.float :balance

      t.timestamps
    end
  end
end
