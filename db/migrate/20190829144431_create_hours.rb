class CreateHours < ActiveRecord::Migration[5.2]
  def change
    create_table :hours do |t|
      t.integer :po_id
      t.string :day
      t.string :desde
      t.string :hasta

      t.timestamps
    end
  end
end
